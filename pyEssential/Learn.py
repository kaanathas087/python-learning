


def main():
    callFun(2,6)

    #list
    x=('hi','this','is','arg','list')
    # ArgList(x)
    # ArgList(*x)

    y=dict(a='1',B='2')
    # kArg(**y)
    # print('this main fun')

    # ########## this is list in python
    x=['this','this','for','list']
    print(','.join(x))
    for s in x:
       print(s,end=' ')


     #         this is dictanary in py
    animal={'cat':'beow','dog':'bow','man':'laugh'}
    for y in animal:
        print(f'{y}       {animal[y]}')
    print(animal)



def callFun(a,b=2,c=4):
    print(a,b,c)

# implementation of list argument
def ArgList(*args):
    for a in args:
        print(a)

# implementation of k word arguments
def kArg(**kwargs):
   for a in kwargs:
      print('thes {} are  {}' .format(a, kwargs[a]))


if __name__ =='__main__':main()


#if __name__ =='__main__':callFun()